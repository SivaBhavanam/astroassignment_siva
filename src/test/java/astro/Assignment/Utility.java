package astro.Assignment;



import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

	public class Utility 
	{
		static WebDriver driver;
		static ExtentReports extent;
	 	static ExtentTest logger;
	 	static String urlPath;
		
	 	public WebDriver getBrowser(String browser) throws Exception{
			
	 		//Check if parameter passed as 'firefox'
			if(browser.equalsIgnoreCase("firefox")){
			
				//set path to firefoxdriver.exe
				System.setProperty("webdriver.firefox.marionette", "Bin/geckodriver.exe");
				
				//create firefox instance
				driver = new FirefoxDriver();
			}
			
			//Check if parameter passed as 'chrome'
			else if(browser.equalsIgnoreCase("chrome")){
				
				//set path to chromedriver.exe
				System.setProperty("webdriver.chrome.driver","Bin/chromedriver.exe");
				
				//create chrome instance
				driver = new ChromeDriver();
			}
			
			//Check if parameter passed as 'ie'
			else if(browser.equalsIgnoreCase("ie")){
				
				//set path to Edge.exe
				System.setProperty("webdriver.ie.driver","Bin/IEDriverServer.exe");
				
				//create ie instance
				driver = new InternetExplorerDriver();
			}
			
			else{
				//If no browser passed throw exception
				throw new Exception("Browser is not correct");
			}
			
			return driver;
	
	}
		//it will capture the screen shot if test failure
	 	public static String getScreenshot(WebDriver driver)
		{
				TakesScreenshot ts=(TakesScreenshot) driver;
				
				File src=ts.getScreenshotAs(OutputType.FILE);
				
				String path=System.getProperty("user.dir")+"/Screenshot/"+System.currentTimeMillis()+".png";
				
				File destination=new File(path);
				
				try 
				{
					FileUtils.copyFile(src, destination);
				} catch (IOException e) 
				{
					System.out.println("Capture Failed "+e.getMessage());
				}
				
				return path;
		}
	 	
	 	
	 	
	}
	
	
	
		
	
