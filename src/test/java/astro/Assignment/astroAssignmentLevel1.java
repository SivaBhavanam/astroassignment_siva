package astro.Assignment;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class astroAssignmentLevel1 extends Utility{
	
	//create global variables 
	WebDriver driver;
	static ExtentReports extent;
	static ExtentTest logger;
	
	//this method will be run before each test method 
	@BeforeMethod
	public void setup()
	{
		// Create Object of ExtentHtmlReporter and provide the path to generate the report 
        // I used current working directory path
		ExtentHtmlReporter reporter=new ExtentHtmlReporter("Reports/AstroHomepage.html");
		
		// Create object of ExtentReports class
	    extent = new ExtentReports();
	    
	    // attach the reporter which we created in above Step 1
	    extent.attachReporter(reporter);
	    
	    // call createTest method and pass the name of TestCase- Based on your requirement
	    logger=extent.createTest("AstroHomepage");
	}
	
	//this test is to verify page loding time & non 200 header urls
	@Test
	public void verifyPagelodTime() throws InterruptedException, Exception {
		
		//instantiate browser by provider browser type(chrome/firefox/ie) using Utility class 
		driver=getBrowser("chrome");
		
		//elements if they are not immediately available WebDriver waits for certain amount of time
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//it will get the current time before navingate to the URL
		long startTime=TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
		
		//instantiate url - string variable to pass as parameter
		String url="http://www.astro.com.my/";
		
		//it will navigate to the mentioned url
		driver.get(url);	
		
		//to maxmize the browser window
		driver.manage().window().maximize();
		
		//it will get the current time once done loading the page
		long finishTime=TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
		
		//calculate the exact time taken to load the page
		long totalTime = finishTime - startTime;
		
		//it will log the test status message to give meaning full reports
		logger.log(Status.INFO, "actual time taken for page load= "+ totalTime);
		
		//checking the page loads within 5 seconds
		if (totalTime<=5){
			logger.log(Status.INFO, "Page Loaded with in 5 seconds");
			
		}else{
			logger.log(Status.INFO, "Page Load time is more than 5 seconds");
		}
		
		//to close the add popup and navigate to home page
		WebElement gotoHomePage= driver.findElement(By.xpath(".//*[@src=\"/sto/images/continue.png\"]"));
		
		gotoHomePage.click();
		
		//it will get the all url links from page
        List<WebElement> links = driver.findElements(By.tagName("a"));
        
        //it will get the all image url links from page and adds to the list
        links.addAll(driver.findElements(By.tagName("img")));
        
        logger.log(Status.INFO, "Total links are "+links.size());
        
        //it will iterate all the url links and gets the empty or not assigned urls
        Iterator<WebElement> checkAllLinks = links.iterator();
        
        while(checkAllLinks.hasNext()){
            
           url = checkAllLinks.next().getAttribute("href");
          
            if(url == null || url.isEmpty()){
            	logger.log(Status.INFO, "URL is empty");
                continue;
            }
            verifyNotActiveLink(url);
        }
        }
	
	//it will iterate all the url links and gets non 200 response url list
    public static void verifyNotActiveLink(String linkUrl)
		{
	        try 
	        {
	           URL url = new URL(linkUrl);
	           
	           HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();
	           
	           httpURLConnect.connect();
	           
	           if(httpURLConnect.getResponseCode()!=200)
	           {
	        	   logger.log(Status.INFO, "Non 200 ResponseCode: "+ linkUrl+" - "+httpURLConnect.getResponseMessage());
	            }
	           
	          if(httpURLConnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND)  
	           {
	               
	        	  logger.log(Status.INFO, "Non 200 ResponseCode: "+ linkUrl+" - "+httpURLConnect.getResponseMessage() + " - "+ HttpURLConnection.HTTP_NOT_FOUND);
	        	  
	        	}
	        } catch (Exception e) {
	           
	        }
			}
			
    //to get failure test screenshots 
    @AfterMethod
   	public void tearDown(ITestResult result) throws IOException
    	{
    		
    		if(result.getStatus()==ITestResult.FAILURE)
    		{
    			String temp=getScreenshot(driver);
    			
    			logger.fail(result.getThrowable().getMessage(), MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
    		}
    		
    		//Flush method will write the test in report- This is mandatory step 
    		extent.flush();
    		driver.quit();
      		
    	}
}
	

