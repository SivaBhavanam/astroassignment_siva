package astro.Assignment;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;



public class astroAssignmentLevel2 extends Utility{
	
	//create global variables 
	ExtentReports extent;
	ExtentTest logger;
	WebDriver driver;
	String currentUrl = "";
	String productTitle = "";
	String price = "";
	
	//this method will be run before each test method 
	@BeforeMethod
	public void setup()
	{	
		// Create Object of ExtentHtmlReporter and provide the path to generate the report 
	    ExtentHtmlReporter reporter=new ExtentHtmlReporter("Reports/Ecommerce.html");
		
	    // Create object of ExtentReports class
	    extent = new ExtentReports();
	    
	    // attach the reporter which we created in above Step 1
	    extent.attachReporter(reporter);
	    
	    // call createTest method and pass the name of TestCase- Based on your requirement
	    logger=extent.createTest("EcommerceTest");
	}
	
	//this test is to verify Ecommerce websites ebay & amazon
	@Test //TestNG annotation
	public void verifyEcommerceEbay() throws Exception  {
		
		//instantiate browser by provider browser type(chrome/firefox/ie) using Utility class 
		driver=getBrowser("chrome");
				
		//elements if they are not immediately available WebDriver waits for certain amount of time
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				
		//it will navigate to the mention website
		driver.get("https://www.ebay.com.my");
		
		//it will maxmize the browser window
		driver.manage().window().maximize();
		
		//it will find the search textbox 
		WebElement serchTextbox=driver.findElement(By.id("gh-ac"));
		
		//it will send/type the mentiond text into textbox
		serchTextbox.sendKeys("iPhone 7");
		
		//it will find the search button
		WebElement searchButton=driver.findElement(By.id("gh-btn"));
		
		//it will click the search button
		searchButton.click();
		
		//it will find the sort menu dropdown 
		WebElement sortAssending=driver.findElement(By.linkText("Best Match"));
		
		//it will clicks the sort menu dropdown button
		sortAssending.click();
		
		//it will find the sort option price highest to low
		WebElement selectAssending=driver.findElement(By.linkText("Price: highest first"));
		
		//select sort option price highest to low
		selectAssending.click();
		
		//Create list for URLS
		List<String> lstUrls = new ArrayList<String>();
		
		//it will create the list of urls which is matching with Apple Iphone 7
		List<WebElement> lstEle = driver.findElements(By.partialLinkText("Apple iPhone 7 "));
		
		//it will iterate all the url links and gets the URL, PRICE, NAME
		for (WebElement element : lstEle)
		    lstUrls.add(element.getAttribute("href"));
	   
		for (String string : lstUrls) {
		    driver.get(string);
		    currentUrl= driver.getCurrentUrl();
		    
		    productTitle= driver.findElement(By.id("itemTitle")).getText();
		    
		try{				   
				price= driver.findElement(By.id("mm-saleDscPrc")).getText();
							   			   
		   }catch(Exception e) {
			   	price= driver.findElement(By.id("prcIsum")).getText();
				   
		   }finally {
		
		   }
		   
			//to generate logs in reports
		   logger.log(Status.INFO, productTitle + "||||      ||||" + "item price:" + price  + "||||      ||||" + currentUrl);
	}
			//once done with above all it will navigate to amazon website
			driver.navigate().to("http:\\www.amazon.in");
			
			verifyEcommerceAmazon();
		
	}
	
		
	public void  verifyEcommerceAmazon() {
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				
		WebElement serchTextbox=driver.findElement(By.name("field-keywords"));
		serchTextbox.sendKeys("iPhone 7");
		
		WebElement searchButton=driver.findElement(By.xpath(".//*[@class='nav-input']"));
		searchButton.click();
		
		WebElement sortDropdown=driver.findElement(By.id("sort"));
		
		Select dropdown= new Select(sortDropdown);
		
		dropdown.selectByVisibleText("Price: High to Low");
		
		List<String> lstUrls = new ArrayList<String>();
		
		List<WebElement> lstEle = driver.findElements(By.partialLinkText("Apple iPhone 7 "));
		
		for (WebElement element : lstEle)
		    lstUrls.add(element.getAttribute("href"));
		
		for (String string : lstUrls) {
		    driver.get(string);
		    currentUrl= driver.getCurrentUrl();
		    
		    productTitle= driver.findElement(By.id("productTitle")).getText();
		    
		   try{
		    	price= driver.findElement(By.xpath("//*[@id=\"priceblock_ourprice\"]")).getText();
		    
		   }catch(Exception e) {
			   logger.log(Status.INFO, "item price is not available");
		   }finally {
			   
		   }
		  		  		
	       logger.log(Status.INFO, productTitle + "||||      ||||" + "item price:" + price  + "||||      ||||" + currentUrl);
	}
	}

	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException
	{
		
		if(result.getStatus()==ITestResult.FAILURE)
		{
			String temp=getScreenshot(driver);
			
			logger.fail(result.getThrowable().getMessage(), MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
		}
		
		extent.flush();
		driver.quit();
		
	}
	
}
	


